-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 25 Sep 2017 pada 11.41
-- Versi Server: 5.5.55-0ubuntu0.14.04.1
-- Versi PHP: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `db_florist`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_ID` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `category_type` varchar(255) NOT NULL,
  PRIMARY KEY (`category_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`category_ID`, `category_name`, `category_type`) VALUES
(1, 'Bouquete', 'Product'),
(2, 'Bunga Parcel', 'Product'),
(3, 'Bunga Ucapan', 'Event'),
(4, 'Pernikahan', 'Event');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_ID` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) NOT NULL,
  `product_category` varchar(255) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_description` text NOT NULL,
  `product_image` text NOT NULL,
  `product_time_duration` datetime NOT NULL,
  `product_count_view` int(11) NOT NULL,
  PRIMARY KEY (`product_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`product_ID`, `product_name`, `product_category`, `product_price`, `product_description`, `product_image`, `product_time_duration`, `product_count_view`) VALUES
(1, 'Bunga Ucapan 1', 'Bunga Ucapan', 400000, 'Lorem ipsum dolar sit amet', 'assets/uploads/CI00017_photo_002.jpg', '0000-00-00 00:00:00', 11),
(2, 'Glory Red Rose', 'Boquete', 800000, 'Lorem ipsum dolar sit amet', 'assets/uploads/KE00004_photo_003.jpg', '0000-00-00 00:00:00', 6),
(3, 'Parcel Buah', 'Bunga Parcel', 500000, 'Lorem ipsum dolar sit amet', 'assets/uploads/CI00025_photo_001.JPG', '0000-00-00 00:00:00', 8);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transacion_detail`
--

CREATE TABLE IF NOT EXISTS `transacion_detail` (
  `detail_ID` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_ID` int(11) NOT NULL,
  `product_ID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `total payment` varchar(255) NOT NULL,
  PRIMARY KEY (`detail_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `transaction_ID` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_status` varchar(255) NOT NULL,
  `transaction_time` datetime NOT NULL,
  `user_ID` int(11) NOT NULL,
  `destination_address` varchar(255) NOT NULL,
  `information` text NOT NULL,
  PRIMARY KEY (`transaction_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `transaction`
--

INSERT INTO `transaction` (`transaction_ID`, `transaction_status`, `transaction_time`, `user_ID`, `destination_address`, `information`) VALUES
(1, 'Konfirmasi Pembayaran', '0000-00-00 00:00:00', 1, 'Jl. Keputih Utara Gg. Mawar no 13', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_telephone` varchar(255) NOT NULL,
  `user_address` text NOT NULL,
  PRIMARY KEY (`user_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_ID`, `user_name`, `user_password`, `user_email`, `user_telephone`, `user_address`) VALUES
(1, 'Dicky Nur Laili', 'e172dd95f4feb21412a692e73929961e', 'nurdicky8@gmail.com', '081513642456', 'Sambogunung Dukun Gresik'),
(2, 'Adam Jhonson', 'e10adc3949ba59abbe56e057f20f883e', 'adam@gmail.com', '085655727695', 'Jalan Gajah Mada Rt 8a');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_admin`
--

CREATE TABLE IF NOT EXISTS `user_admin` (
  `admin_ID` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  PRIMARY KEY (`admin_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user_admin`
--

INSERT INTO `user_admin` (`admin_ID`, `admin_name`, `admin_password`) VALUES
(1, 'admin', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
