<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Cart extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('category_model', 'product_model', 'transaction_model', 'transaction_detail_model'));
    }

    public function add_post(){
        $status = $this->input->post('transaction_status');
        $time = $this->input->post('transaction_time');
        $userid = $this->input->post('user_ID');
        $address = $this->input->post('destination_address');

        $input = array(
                'transaction_status' => $status,
                'transaction_time' => $time,
                'user_ID' => $userid,
                'destination_address' => $address,
            );

        $data = $this->transaction_model->get_by_id($userid);
        if(!empty ($data)){
            $message = [
                'error' => false,
                'message' => 'Data berhasil ditambahkan ke keranjang',
                'cart' => $data
            ];
        }else{
            $message = [
                'error' => true,
                'message' => 'Transaksi gagal ditambahkan !'
            ];
        }
        $this->response($message, REST_Controller::HTTP_CREATED);
    }


    public function list_get(){
        $data = $this->transaction_detail_model->get_all();

        foreach ($data as $transaksi) {
          $result[] = array(
            'detail_ID' => $transaksi->detail_ID,
            'transaction_ID' => $this->transaction_model->get_by_id($transaksi->transaction_ID),
            'prodcut_ID' => $this->product_model->get_by_id($transaksi->product_ID),
            'quantity' => $transaksi->quantity,
            'price' => $transaksi->price,
            'total_payment' => $transaksi->total_payment
          );
        }

        if(!empty ($data)){
            $message = [
                'error' => false,
                'message' => 'Data transaksi',
                'cart' => $result
            ];
        }else{
            $message = [
                'error' => true,
                'message' => 'Transaksi masih kosong !'
            ];
        }
        $this->response($message, REST_Controller::HTTP_CREATED);
    }

    public function status_get($param){
      $data = $this->transaction_detail_model->get_by_status($param);

      if(!empty ($data)){
          $message = [
              'error' => false,
              'message' => 'Data transaksi',
              'cart' => $result
          ];
      }else{
          $message = [
              'error' => true,
              'message' => 'Transaksi masih kosong !'
          ];
      }
      $this->response($message, REST_Controller::HTTP_CREATED);
    }
}
